<?php
/*
Plugin Name: ELementor widgets - ThemeXclub
Plugin URI: https://github.com/kamalahmed/txc-elementor-widgets
Description: A sample plugin for adding awesome widgets to elementor page builder
Version: 1.0.0
Author: Kamal Ahmed
Author URI: http://kamaldeveloper.com
License: GPLv2 or later
Text Domain: txc-elementor-widgets
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright  Kamal Ahmed.
*/

// Make sure we don't expose any info if called directly
defined('ABSPATH') || die('Direct Access is not allowed!');

define( 'TXC_ELEM_WIDGET_VERSION', '1.0.0' );
define( 'TXC_ELEM_TEXTDOMAIN', 'txc-elementor-widgets' );
define( 'TXC_ELEM_WIDGET_MIN_WP_VERSION', '4.9' );
define( 'TXC_ELEM_WIDGET_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'TXC_ELEM_WIDGET_PLUGIN_URI', plugin_dir_url( __FILE__) );

/**
 * Main Elementor Test Extension Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class TXC_Elementor_Test_Extension {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.0';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var TXC_Elementor_Test_Extension The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 *
	 * @return TXC_Elementor_Test_Extension An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'init', [ $this, 'i18n' ] );
		add_action( 'plugins_loaded', [ $this, 'init' ] );


	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( TXC_ELEM_TEXTDOMAIN );

	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
			return;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return;
		}

		// Add Plugin actions
		add_action( 'elementor/elements/categories_registered', [$this, 'txc_add_elementor_widget_categories'] );
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );
		//add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );
		
		// add scrippts 
		add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);

	}

	public function enqueue_scripts(  )
	{
		wp_enqueue_style( 'txc-testimonial-style', "//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" );
		wp_enqueue_style( 'txc-testimonial-bootstrap', TXC_ELEM_WIDGET_PLUGIN_URI .'style.css' );
		wp_enqueue_script( 'txc-testimonial-bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '4.0.0', true);
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
		/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', TXC_ELEM_TEXTDOMAIN ),
			'<strong>' . esc_html__( 'Elementor Test Extension', TXC_ELEM_TEXTDOMAIN ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', TXC_ELEM_TEXTDOMAIN ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
		/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', TXC_ELEM_TEXTDOMAIN ),
			'<strong>' . esc_html__( 'Elementor Test Extension', TXC_ELEM_TEXTDOMAIN ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', TXC_ELEM_TEXTDOMAIN ) . '</strong>',
			self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
		/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', TXC_ELEM_TEXTDOMAIN ),
			'<strong>' . esc_html__( 'Elementor Test Extension', TXC_ELEM_TEXTDOMAIN ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', TXC_ELEM_TEXTDOMAIN ) . '</strong>',
			self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		// Include Widget files
		require_once( __DIR__ . '/widgets/class-txc-widget.php' );

		// Register widget
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\TXC_Elementor_widget() );

	}


	/**
	 * Add a new category to elementor editor for our theme
	 * @param \Elementor\Elements_Manager $elements_manager
	 */
	public function txc_add_elementor_widget_categories( $elements_manager ) {

		$elements_manager->add_category(
			'themexclub-category',
			[
				'title' => __( 'ThemeXclub ADDONS', TXC_ELEM_TEXTDOMAIN ),
				'icon' => 'fa fa-plug',
			]
		);

	}



}

TXC_Elementor_Test_Extension::instance();


