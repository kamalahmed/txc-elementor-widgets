<?php
namespace Elementor;
/**
 * Elementor Testimonial Widget.
 *
 * Elementor widget that inserts an Testimonial content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class TXC_Elementor_widget extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve testimonial widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'txc-testimonial';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve testimonial widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Testimonial', TXC_ELEM_TEXTDOMAIN );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve testimonial widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-user';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the testimonial widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'themexclub-category' ];
	}

	/**
	 * Register testimonial widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', TXC_ELEM_TEXTDOMAIN ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'testimonial_body',
			[
				'label' => __( 'Testimonial body', TXC_ELEM_TEXTDOMAIN ),
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter a some text ', TXC_ELEM_TEXTDOMAIN ),
			]
		);

		$this->add_control(
			'testimonial_name',
			[
				'label' => __( 'Name', TXC_ELEM_TEXTDOMAIN ),
				'type' => Controls_Manager::TEXT,
				'input_type' => 'text',
				'placeholder' => __( 'Eg. John Doe', TXC_ELEM_TEXTDOMAIN ),
			]
		);

		$this->add_control(
			'testimonial_designation',
			[
				'label' => __( 'Designation', TXC_ELEM_TEXTDOMAIN ),
				'type' => Controls_Manager::TEXT,
				'input_type' => 'text',
				'placeholder' => __( 'Eg. Product Manager at Apple', TXC_ELEM_TEXTDOMAIN ),
			]
		);

		$this->add_control(
			'testimonial_image',
			[
				'label' => esc_html__( 'Testimonial Image', TXC_ELEM_TEXTDOMAIN ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'testimonial_image_size',
				'default' => 'full',
			]
		);

		$this->end_controls_section();

        /*social profile*/
		$this->start_controls_section(
			'testimonial_social_profiles',
			[
				'label' => esc_html__( 'Social Profiles', TXC_ELEM_TEXTDOMAIN )
			]
		);

		$this->add_control(
			'testimonial_enable_social_profiles',
			[
				'label' => esc_html__( 'Display Social Profiles?', TXC_ELEM_TEXTDOMAIN ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);


		$this->add_control(
			'testimonial_social_profile_links',
			[
				'type' => Controls_Manager::REPEATER,
				'condition' => [
					'testimonial_enable_social_profiles!' => '',
				],
				'default' => [
					[
						'social' => 'fa fa-facebook',
					],
					[
						'social' => 'fa fa-twitter',
					],
					[
						'social' => 'fa fa-google-plus',
					],
					[
						'social' => 'fa fa-linkedin',
					],
				],
				'fields' => [
					[
						'name' => 'social',
						'label' => esc_html__( 'Icon', TXC_ELEM_TEXTDOMAIN ),
						'type' => Controls_Manager::ICON,
						'label_block' => true,
						'default' => 'fa fa-wordpress',
						'include' => [
							'fa fa-apple',
							'fa fa-behance',
							'fa fa-bitbucket',
							'fa fa-codepen',
							'fa fa-delicious',
							'fa fa-digg',
							'fa fa-dribbble',
							'fa fa-envelope',
							'fa fa-facebook',
							'fa fa-flickr',
							'fa fa-foursquare',
							'fa fa-github',
							'fa fa-google-plus',
							'fa fa-houzz',
							'fa fa-instagram',
							'fa fa-jsfiddle',
							'fa fa-linkedin',
							'fa fa-medium',
							'fa fa-pinterest',
							'fa fa-product-hunt',
							'fa fa-reddit',
							'fa fa-shopping-cart',
							'fa fa-slideshare',
							'fa fa-snapchat',
							'fa fa-soundcloud',
							'fa fa-spotify',
							'fa fa-stack-overflow',
							'fa fa-tripadvisor',
							'fa fa-tumblr',
							'fa fa-twitch',
							'fa fa-twitter',
							'fa fa-vimeo',
							'fa fa-vk',
							'fa fa-whatsapp',
							'fa fa-wordpress',
							'fa fa-xing',
							'fa fa-yelp',
							'fa fa-youtube',
						],
					],
					[
						'name' => 'link',
						'label' => esc_html__( 'Link', TXC_ELEM_TEXTDOMAIN ),
						'type' => Controls_Manager::URL,
						'label_block' => true,
						'default' => [
							'url' => '',
							'is_external' => 'true',
						],
						'placeholder' => esc_html__( 'Place URL here', TXC_ELEM_TEXTDOMAIN ),
					],
				],
				'title_field' => '<i class="{{ social }}"></i> {{{ social.replace( \'fa fa-\', \'\' ).replace( \'-\', \' \' ).replace( /\b\w/g, function( letter ){ return letter.toUpperCase() } ) }}}',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render testimonial widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();
		//error_log(print_r( $settings['testimonial_image'], true) );
		$testimonial_image = $this->get_settings( 'testimonial_image' );
		$testimonial_image_url = Group_Control_Image_Size::get_attachment_image_src( $testimonial_image['id'], 'testimonial_image_size', $settings );

		?>
		<div class="container" style="background:#ebebeb;margin:35px;">
			<!-- EXAMPLE 1 -->
			<div class="parent-box">
				<div class="box">
					<div class="dialog-box">
						<p>
							<a href="#">
								<?php echo !empty( $settings['testimonial_body'] ) ? $settings['testimonial_body'] : ''; ?>
							</a>
						</p>
						<p>
                    <span>
                        <?php echo !empty( $settings['testimonial_designation'] ) ? $settings['testimonial_designation'] : ''; ?>
                        <br>
	                    <?php if ( ! empty( $settings['testimonial_enable_social_profiles'] ) ): ?>
					<?php foreach ( $settings['testimonial_social_profile_links'] as $item ) : ?>
						<?php if ( ! empty( $item['social'] ) ) : ?>
							<?php $target = $item['link']['is_external'] ? ' target="_blank"' : ''; ?>
								<a href="<?php echo esc_attr( $item['link']['url'] ); ?>"<?php echo $target; ?>><i class="<?php echo esc_attr($item['social'] ); ?>"></i></a>
						<?php endif; ?>
					<?php endforeach; ?>
	                    <?php endif; ?>
<!--                        <i class="fa fa-facebook" aria-hidden="true"></i>-->
<!--                        <i class="fa fa-twitter" aria-hidden="true"></i>-->
                    </span>
						</p>
					</div>
					<div class="info">
						<div style="width:50%;float:left;">
							<a href="#"><?php echo !empty( $settings['testimonial_name'] ) ? $settings['testimonial_name'] : ''; ?></a>
						</div>
						<?php if ( !empty(  $testimonial_image_url) ){ ?>
							<div>
								<img style="max-height:100%;" src="<?php echo esc_attr( esc_url($testimonial_image_url)); ?>" />
							</div>
						<?php } ?>
						
					</div>
				</div>
			</div> <!--this div should be repeated-->

		</div>

		<?php

	}

}
